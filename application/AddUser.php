<?php

use frame\Frame;
use frame\widgets\Form;
use frame\widgets\Html;

require_once 'Frame/Initialise.php';

Frame::$app->run('User', 'Add');

Frame::$app->view->title = "Add Users";


$user = new models\User();

?>

<?= Frame::$app->view->begin(); ?>
<div>

    <?php $form = (new Form())->begin('', 'get', ['data-parsley-validate'=>'']); ?>

        <?= $form->field($user, 'forename', [])->textInput(); ?>
        <?= Html::submitButton(); ?>

    <?php $form->end(); ?>

</div>
<?= Frame::$app->view->end(); ?>