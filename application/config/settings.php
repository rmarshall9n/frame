<?php

return [
    'logger' => [
        'name' => 'FrameLog',
        'useSentry' => true,
        'sentryDSN' => 'https://6b57681ceab34042bc17943236117744:898db470ed914552a4a8e7dc57394acc@sentry.io/126883',
    ],
    'errorHandler' => [
        'level' => E_ALL,
        'errorPage' => 'views/errors/error.php',
    ]
];