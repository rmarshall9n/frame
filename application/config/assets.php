<?php

return [
    'js' => [
        '/jquery/jquery.min.js',
        '/bootstrap/bootstrap.min.js',
        '/parsley/parsley.min.js',
    ],
    'css' => [
        '/bootstrap/bootstrap.min.css',
        '/frame.css'
    ],
];