<?php

use frame\Frame;
use frame\widgets\BootstrapWidgets as Widgets;
require_once 'Frame/Initialise.php';

Frame::$app->run('Test', 'logger');

Frame::$app->view->title = "Logger Test";
Frame::$app->view->addBreadcrumbs([
    'Home' => 'index.php',
    'Logger Test' => null
]);


Frame::logger()->critical('Crit');
Frame::logger()->error('Bad');
Frame::logger()->warning('warning warning!');
Frame::logger()->info('Some info.');
Frame::logger()->debug('Some debug.');

?>
<?= Frame::$app->view->begin(); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p>logger()->critical('Crit');</p>
            <p>logger()->error('Bad');</p>
            <p>logger()->warning('warning warning!')</p>
            <p>logger()->info('Some info.')</p>
            <p>logger()->debug('Some debug.')</p>
        </div>
    </div>
</div>

<?= Frame::$app->view->end(); ?>