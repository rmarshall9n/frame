<?php

namespace models;

use frame\model;

/**
*
*/
class User extends Model
{
    public $columns = [
        'forename' => 'Forename',
        'surname' => 'Surname',
        'dateOfBirth' => 'Date of Birth',
        'email' => 'Email',
    ];

    public static function attributeLabels()
    {
        return [
            'forename' => 'Forename',
            'surname' => 'Surname',
            'dateOfBirth' => 'Date of Birth',
            'email' => 'Email',
        ];
    }

    public static function rules()
    {
        return [
            'required' => 'forename',
            'lengthMax' => [['forename', 10], ['surname', 10]],
            'integer' =>  'dateOfBirth'
        ];
    }

    function __construct()
    {

    }


}