<?php

namespace frame;

use frame\view;

/**
* The main application class
*/
class Application
{
    private $appName = "FRAME";

    public $view;

    private $config;

    function __construct($config = [])
    {
        $this->config = $config;
        Frame::$app = $this;
    }

    public function init()
    {
        // validate config
        $this->view = new View();
        $this->view->assetManager->loadPackage($this->config['assets']);
    }

    public function getConfig($for = '')
    {
        if(empty($for)) {
            return $this->config;
        }

        return isset($this->config[$for]) ? $this->config[$for] : [];
    }

    public static function run($controller, $action = null)
    {
        // check if the controller exists and create it
        $controller = '\\controllers\\' . $controller . "Controller";
        if(!class_exists($controller)) {
            throw new \Exception("Application Error: Controller ({$controller}) not found.");
        }
        $controller = new $controller();

        // create the correct action and check it exists
        $action = (($action === null) ? $controller->defaultAction : $action) . 'Action';

        if(!is_callable([$controller, $action])) {
            throw new \Exception("Application Error: {$controller->getClassName()} does not have action ({$action}).");
        }

        // call before, action, after
        $controller->before();
        $data = $controller->$action();
        $controller->after();

        // extract data into global space
        if(!empty($data) && is_array($data)) {
            foreach ($data as $key => $value) {
                $GLOBALS[$key] = $value;
            }
        }
    }

    public function getAppName()
    {
        return $this->appName;
    }
}