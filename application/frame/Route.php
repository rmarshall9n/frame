<?php

/**
* Temp Route class
*/
class Route extends AnotherClass
{

    function __construct()
    {
    }


    public function resolveRoute()
    {
        return ['controller', 'action'];
    }
}