<?php

namespace frame;

/**
* Asset loader class
*/
class Asset
{
    const VIEW_HEAD  = 1;
    const VIEW_BODY_BEGIN  = 2;
    const VIEW_BODY_END  = 3;

    public $cssDir = '/web/css';
    public $jsDir = '/web/js';

    public $jsFiles;
    public $cssFiles;
    public $jsCode;
    public $cssCode;

    function __construct()
    {
        $this->clearAssets();
    }



    function setCssDir($cssDir)
    {
        $this->cssDir = $cssDir;
    }

    function setJsDir($jsDir)
    {
        $this->jsDir = $jsDir;
    }


    public function loadPackage($assets)
    {
        foreach ($assets['css'] as $key => $path) {
            $this->loadCssFile(Frame::dir('css') . $path);
        }

        foreach ($assets['js'] as $key => $path) {
            $this->loadJsFile(Frame::dir('js') . $path);
        }
    }


    public function loadJsCode($code, $position = self::VIEW_HEAD)
    {
        $this->jsCode[self::VIEW_HEAD][] = $code;
    }

    public function loadJsFile($file, $position = self::VIEW_HEAD)
    {
        $this->jsFiles[self::VIEW_HEAD][] = $file;
    }

    public function loadCssCode($code)
    {
        $this->cssCode[] = $code;
    }

    public function loadCssFile($file)
    {
        $this->cssFiles[] = $file;
    }

    public function getJsCode($position)
    {
        $code = "";
        if(!empty($this->jsCode[$position])) {
            foreach ($this->jsCode[$position] as $block) {
                $code .= "\n<script type=\"text/javascript\">\n" . $block . "\n</script>\n";
            }
        }
        return $code;
    }

    public function getJsFiles($position)
    {
        $code = "";
        if(!empty($this->jsFiles[$position])) {
            foreach ($this->jsFiles[$position] as $src) {
                $code .= "<script type=\"text/javascript\" src=\"" . $src . "\"></script>\n";
            }
        }
        return $code;
    }

    public function getCssFiles()
    {
        $code = "";
        if(!empty($this->cssFiles)) {
            foreach ($this->cssFiles as $src) {
                $code .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . $src . "\">\n";
            }
        }
        return $code;
    }

    public function getCssCode()
    {
        $code = "";
        if(!empty($this->cssCode)) {
            foreach ($this->cssCode as $block) {
                $code .= "\n<style>\n" . $block . "\n</style>\n";
            }
        }
        return $code;
    }

    public function clearAssets()
    {
        $jsFiles = [];
        $cssFiles = [];
        $jsCode = [];
        $cssCode = [];
    }
}