<?php

namespace frame\validators;

/**
* Parsley validation validator
*/
class ClientParsleyValidator extends ClientBaseValidator
{
    public function required()
    {
        return ['data-parsley-required' => 'true'];
    }

    public function min($value)
    {
        return ['data-parsley-min' => $value];
    }

    public function max($value)
    {
        return ['data-parsley-max' => $value];
    }

    public function minLen($value)
    {
        return ['data-parsley-minlength' => $value];
    }

    public function maxLen($value)
    {
        return ['data-parsley-maxlength' => $value];
    }
}