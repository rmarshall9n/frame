<?php

namespace frame\validators;

/**
* Client Base Validator
*/
class ClientBaseValidator implements ClientValidator
{
    public function required()
    {
        return ['required' => null];
    }

    public function min($value)
    {
        return ['min' => $value];
    }

    public function max($value)
    {
        return ['max' => $value];
    }

    public function minLen($value)
    {
        return ['minlength' => $value];
    }

    public function maxLen($value)
    {
        return ['maxlength' => $value];
    }
}