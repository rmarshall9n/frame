<?php

namespace frame\validators;

/**
* Valitron Validator
*/
class BaseValitronValidator implements BaseValidator
{
    private $validator;

    public function init($data)
    {
        $this->validator = new \Valitron\Validator($data);
    }

    public function setRules($rules)
    {
        $this->validator->rules($rules);
    }

    public function validate()
    {
        return $this->validator->validate();
    }

    public function getErrors()
    {
        return $this->validator->errors();
    }

    public function getRuleForAttribute($rules, $attribute)
    {
        $attributeRules = [];
        foreach ($rules as $rule => $params) {
            if(is_array($params)) {
                foreach ($params as $attrParam) {
                    if($attrParam[0] == $attribute) {
                        $attributeRules[$rule] = $attrParam[1];
                    }
                }
            } elseif ($params == $attribute) {
                $attributeRules[$rule] = null;
            }
        }

        return $attributeRules;
    }

    function generateClientRules($attributeRules, $clientValidator)
    {
        $clientRules = [];

        foreach ($attributeRules as $rule => $value) {
            switch ($rule) {
                case 'required':
                    $validator = $clientValidator->required();
                    break;
                case 'min':
                    $validator = $clientValidator->min($value);
                    break;
                case 'max':
                    $validator = $clientValidator->max($value);
                    break;
                case 'lengthMin':
                    $validator = $clientValidator->minLen($value);
                    break;
                case 'lengthMax':
                    $validator = $clientValidator->maxLen($value);
                    break;
                default:
                    // unsupported validator
                    $validator = null;
                    break;
            }
            if(is_array($validator)) {
                $clientRules[key($validator)] = current($validator);
            }
        }

        return $clientRules;
    }
}