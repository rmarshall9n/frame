<?php

namespace frame\validators;

/**
* Base Validator Interface
*/
interface BaseValidator
{
    public function init($data);
    public function setRules($rules);
    public function validate();
    public function getErrors();
    public function getRuleForAttribute($rules, $attribute);
    public function generateClientRules($attributeRules, $clientValidator);
}