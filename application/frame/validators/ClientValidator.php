<?php

namespace frame\validators;

/**
* Client Validator Interface
*/
interface ClientValidator
{
    public function required();
    public function min($value);
    public function max($value);
    public function minLen($value);
    public function maxLen($value);
}