<?php

namespace frame\base;

/**
* interface for an error handler
*/
interface ErrorHandlerInterface
{
    public function init();
    public function get();
}
