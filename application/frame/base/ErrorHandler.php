<?php

namespace frame\base;

use frame\Frame;

/**
* Error handler for development and production environments
*/
class ErrorHandler implements ErrorHandlerInterface
{
    protected $whoops;
    protected $level;
    protected $errorPage;

    function __construct($config = [])
    {
        $this->level = (isset($config['level'])) ? $config['level'] : E_ALL;
        $this->errorPage = (isset($config['errorPage'])) ? $config['errorPage'] : Frame::dir('view').'/errors/error.php';
    }

    public function init()
    {
        // turn on all errors
        error_reporting($this->level);

        $this->whoops = new \Whoops\Run;
        $this->whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());

        if(FRAME_ENV == ENV_DEV) {
            set_error_handler(array($this, "handlerDevelopment"));
        } else {
            set_error_handler(array($this, "handlerProduction"));
        }

        set_exception_handler(array($this, "customExceptionHandler"));
    }

    public function get()
    {
        return $this;
    }

    // we dont want to kill the page because of a warning so silence those from whoops and re-enable them in php
    // $whoops->silenceErrorsInPaths('/.*/', E_STRICT | E_DEPRECATED | E_WARNING);
    // set_error_handler(null, E_STRICT | E_DEPRECATED | E_WARNING);

    function mapErrorCode($level)
    {
        $error = null;
        switch ($level) {
            case E_PARSE:
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case E_USER_ERROR:
                $error = 'Fatal Error';
                break;
            case E_WARNING:
            case E_USER_WARNING:
            case E_COMPILE_WARNING:
            case E_RECOVERABLE_ERROR:
                $error = 'Warning';
                break;
            case E_NOTICE:
            case E_USER_NOTICE:
                $error = 'Notice';
                break;
            case E_STRICT:
                $error = 'Strict';
                break;
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                $error = 'Deprecated';
                break;
            default :
                break;
        }

        return $error;
    }

    public function handlerDevelopment($level, $message, $file, $line)
    {
        // This error code is not included in error_reporting, so let it fall
        // through to the standard PHP error handler
        if (!(error_reporting() & $level)) {
            return false;
        }

        $disableDefault = true;

        switch ($level) {
            case E_PARSE:
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case E_USER_ERROR:
                $error = 'Fatal Error';
                $this->whoops->handleError($level, $message, $file, $line);
                break;
            case E_WARNING:
            case E_USER_WARNING:
            case E_COMPILE_WARNING:
            case E_RECOVERABLE_ERROR:
                $error = 'Warning';
                // $this->whoops->handleError($level, $message, $file, $line);
                $disableDefault = false;
                break;
            case E_NOTICE:
            case E_USER_NOTICE:
                $error = 'Notice';
                // $this->whoops->handleError($level, $message, $file, $line);
                $disableDefault = false;
                break;
            case E_STRICT:
                $error = 'Strict';
                // $this->whoops->handleError($level, $message, $file, $line);
                $disableDefault = false;
                break;
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                $error = 'Deprecated';
                // $this->whoops->handleError($level, $message, $file, $line);
                $disableDefault = false;
                break;
            default :
                break;
        }

        // Don't execute PHP internal error handler
        return $disableDefault;
    }

    public function handlerProduction($level, $message, $file, $line)
    {
        // This error code is not included in error_reporting, so let it fall
        // through to the standard PHP error handler
        if (!(error_reporting() & $level)) {
            return false;
        }

        switch ($level) {
            case E_PARSE:
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case E_USER_ERROR:
                throw new frame\error\Exception("pam", 500);
                break;
            default:
                break;
        }

        // Don't execute PHP internal error handler
        return true;
    }

    public function customExceptionHandler($exception)
    {
        if ($exception instanceof \frame\error\HttpException) {
            $this->displayHttpException($exception->getMessage(), $exception->getStatusCode());
        } else if ($exception instanceof \frame\error\UserException) {
            $this->displayError($exception->getMessage());
        } else if (FRAME_ENV == ENV_DEV) {
            $this->whoops->handleException($exception);
        } else {
            $this->displayError("An unrecoverable error occurred.");
        }

        exit;
    }

    public function displayError($message = "", $statusCode = 500)
    {
        http_response_code($statusCode);
        include $this->errorPage;
        exit;
    }

    public function displayHttpException($message, $statusCode = 500)
    {
        http_response_code($statusCode);
        include $this->errorPage;
        exit;
    }
}
