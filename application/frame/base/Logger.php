<?php

namespace frame\base;

/**
* Logger
*/
class Logger implements LoggerInterface
{
    private $path = "";
    private $name = "";
    private $logger = null;

    function __construct($path, $config = [])
    {
        $this->path = $path;
        $this->name = empty($config['name']) ? 'Log' : $config['name'];
    }

    public function init()
    {
        $this->logger = new \Monolog\Logger($this->name, [], [], null);

        // process information
        $this->logger->pushProcessor(new \Monolog\Processor\WebProcessor);

        // debug, info to console
        $this->logger->pushHandler(new \Monolog\Handler\BrowserConsoleHandler(\Monolog\Logger::DEBUG, false));

        // warnings to file
        $this->logger->pushHandler(new \Monolog\Handler\StreamHandler($this->path . '/warnings.log', \Monolog\Logger::WARNING, false));

        // errors to file
        $errorHandler = new \Monolog\Handler\StreamHandler($this->path . '/error.log', \Monolog\Logger::ERROR, false);
        $errorHandler->setFormatter(new \Monolog\Formatter\HtmlFormatter);
        $this->logger->pushHandler($errorHandler);

        // if sentry key and swithced on, log everything there too
        if(isset($config['sentryDSN']) && !(isset($config['useSentry']) && $config['useSentry'] === false)) {
            $handler = new \Monolog\Handler\RavenHandler(new \Raven_Client($config['sentryDSN']));
            $handler->setFormatter(new \Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));
            $this->logger->pushHandler($handler);
        }

        $l = new \Monolog\Logger('error');
        $l->pushHandler(new \Monolog\Handler\StreamHandler($this->path . '/warnings.log', \Monolog\Logger::WARNING, false));
        $l->pushHandler(new \Monolog\Handler\StreamHandler($this->path . '/error.log', \Monolog\Logger::ERROR, false));
        \Monolog\ErrorHandler::register($l);
    }

    public function get()
    {
        return $this->logger;
    }
}