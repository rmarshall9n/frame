<?php

namespace frame\base;

/**
* interface for an loggers
*/
interface LoggerInterface
{
    public function init();
    public function get();
}
