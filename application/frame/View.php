<?php

namespace frame;

use frame\Frame;
use frame\Asset;

/**
* view class
*/
class View
{
    const FRAME_VIEW_HEAD = '<![CDATA[FRAME-VIEW-HEAD]]>';
    const FRAME_VIEW_BODY_BEGIN = '<![CDATA[FRAME-VIEW-BODY-BEGIN]]>';
    const FRAME_VIEW_BODY_END = '<![CDATA[FRAME-VIEW-BODY-END]]>';

    public $title;
    public $template = 'default';

    public $breadcrumbs;
    public $assetManager;

    function __construct()
    {
        $this->breadcrumbs = [];
        $this->assetManager = new Asset();
    }

    public function begin()
    {
        require Frame::dir('view') . '/layouts/' . $this->template . 'Begin.php';
    }

    public function end()
    {
        require Frame::dir('view') . '/layouts/' . $this->template . 'End.php';

        $content = ob_get_clean();

        echo strtr($content, [
            self::FRAME_VIEW_HEAD => $this->insertHeadHtml(),
            self::FRAME_VIEW_BODY_BEGIN => $this->insertBodyBeginHtml(),
            self::FRAME_VIEW_BODY_END => $this->insertBodyEndHtml(),
        ]);

        $this->assetManager->clearAssets();
    }

    public function insertHeadHtml()
    {
        return
            $this->assetManager->getCssFiles() .
            $this->assetManager->getCssCode() .
            $this->assetManager->getJsFiles(Asset::VIEW_HEAD) .
            $this->assetManager->getJsCode(Asset::VIEW_HEAD);
    }

    public function insertBodyBeginHtml()
    {
        return $this->assetManager->getJsCode(Asset::VIEW_BODY_BEGIN);
    }

    public function insertBodyEndHtml()
    {
        return $this->assetManager->getJsCode(Asset::VIEW_BODY_END);
    }

    public function hookHead()
    {
        echo  self::FRAME_VIEW_HEAD;
    }

    public function hookBegin()
    {
        echo  self::FRAME_VIEW_BODY_BEGIN;
    }

    public function hookEnd()
    {
        echo  self::FRAME_VIEW_BODY_END;
    }

    public function addBreadcrumbs($breadcrumbs)
    {
        $this->breadcrumbs = array_merge($this->breadcrumbs, $breadcrumbs);
    }
}
