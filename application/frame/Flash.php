<?php

namespace frame;

/**
* flash class
*/
class Flash
{

    function __construct()
    {
    }

    public function error($message)
    {
        echo "Flash error: " . $message;
    }

    public function warning($message)
    {
        echo "Flash warning: " . $message;
    }

    public function success($message)
    {
        echo "Flash success: " . $message;
    }

    public function info($message)
    {
        echo "Flash info: " . $message;
    }
}