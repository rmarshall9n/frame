<?php

use frame\Frame;
use frame\Application;

define('ENV_DEV', 1);
define('ENV_PROD', 2);
defined('FRAME_DEBUG') or define('FRAME_DEBUG', false);
defined('FRAME_ENV') or define('FRAME_ENV', ENV_PROD);
// defined('FRAME_ENV') or define('FRAME_ENV', ENV_DEV);


require_once __DIR__ . '/../../vendor/autoload.php';

$config = require __DIR__ . '/../config/settings.php';
$config['assets'] = require __DIR__ . '/../config/assets.php';

Frame::init(new Application($config));
