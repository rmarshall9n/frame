<?php

namespace frame;

use frame\Object;

/**
* Base Controller Class
*/
class Controller extends Object
{
    public $defaultAction = "index";

    function __construct()
    {
    }


    public function access($value='')
    {
        # code...
    }

    public function before()
    {
        // echo "before";
    }

    public function after()
    {
        // echo "after";
    }

    public function action()
    {
    }

    public function checkAccess()
    {
        return true;
    }
}