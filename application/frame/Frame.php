<?php

namespace frame;

use frame\Log;
use Dice\Dice;

/**
* Frame bootstrap class
*/
class Frame
{
    public static $app;
    public static $di;
    private static $logger;
    private static $errorHandler;

    function __construct()
    {
    }

    public static function init($application)
    {
        self::$app = $application;

        // init dependency injection container setup
        self::$di = new Dice();

        // initialise logger
        self::$logger = new base\Logger(self::dir('logs'), self::$app->getConfig('logger'));
        self::$logger->init();

        self::$errorHandler = new base\ErrorHandler(self::$app->getConfig('errorHandler'));
        self::$errorHandler->init();

        // initialise the application
        self::$app->init();
    }

    public static function logger()
    {
        return self::$logger->get();
    }

    public static function error()
    {
        return self::$errorHandler->get();
    }

    public static function dir($to)
    {
        $root = dirname(__DIR__);
        $web = '';

        $dir = [
            'web' => $web,
            'view' => $web . 'views',
            'js' => $web . 'web'.DIRECTORY_SEPARATOR .'js',
            'css' => $web . 'web'.DIRECTORY_SEPARATOR .'css',
            'fonts' => $web . 'web'.DIRECTORY_SEPARATOR .'fonts',
            'logs' => $root . DIRECTORY_SEPARATOR  .'logs',
            'root' => $root,
        ];

        return isset($dir[$to]) ? $dir[$to] : '';
    }

    public function buildPath($structure = [])
    {
        return implode(DIRECTORY_SEPARATOR, $structure);
    }

    public static function dump($value='')
    {
        echo "<pre>";
        var_dump($value);
        echo "</pre>";
    }

    public static function dumpd($value='')
    {
        self::dump($value);
        die();
    }
}