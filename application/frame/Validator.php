<?php

namespace frame;

use frame\validators\BaseValitronValidator;
// use frame\validators\ClientBaseValidator;
use frame\validators\ClientParsleyValidator;

/**
* Validator
*/
class Validator
{
    private $baseValidator;
    private $clientValidator;

    function __construct()
    {
        $this->baseValidator = new BaseValitronValidator();
        // $this->clientValidator = new ClientBaseValidator();
        $this->clientValidator = new ClientParsleyValidator();
    }

    public function validateAttributes($model, $attributeNames)
    {
        $this->validate($model->getAttributes($attributeNames), $model->rules());
        $model->addErrors($this->baseValidator->getErrors());
    }

    public function validate($data, $rules)
    {

        $this->baseValidator->init($data);
        $this->baseValidator->setRules($rules);
        $this->baseValidator->validate();

        return $this->baseValidator;
    }

    public function getClientValidation($model, $attribute)
    {
        $rules = $model->rules();
        $rules = $this->baseValidator->getRuleForAttribute($rules, $attribute);
        return $this->baseValidator->generateClientRules($rules, $this->clientValidator);
    }

}