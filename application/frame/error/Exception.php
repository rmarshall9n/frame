<?php

namespace frame\error;

/**
*
*/
class Exception extends \Exception
{
    function __construct($message, $code)
    {
        parent::__construct($message, $code);
    }
}