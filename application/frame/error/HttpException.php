<?php

namespace frame\error;

/**
*
*/
class HttpException extends Exception
{
    private $statusCode;

    function __construct($statusCode, $message, $code = 0)
    {
        $this->statusCode = $statusCode;
        parent::__construct($message, $code);
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }
}