<?php

namespace frame\error;

/**
*
*/
class ForbiddenException extends HttpException
{
    function __construct($message = "Sorry, You don't have permission to access that.", $code = 0)
    {
        parent::__construct(403, $message, $code);
    }
}