<?php

namespace frame\error;

/**
*
*/
class NotFoundException extends HttpException
{
    function __construct($message = "Sorry, We couldn't find what you were looking for.", $code = 0)
    {
        parent::__construct(404, $message, $code);
    }
}