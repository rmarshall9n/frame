<?php

namespace frame\widgets;

/**
* Widgets interface
*/
interface IWidgets
{
    public static function breadcrumbs($breadcrumbs);
    public static function panelBegin($heading, $options = []);
    public static function panelEnd();

}