<?php

namespace frame\widgets;

use frame\widgets\Field;
use frame\widgets\Html;

/**
* Form templates
*/
class Form
{

    function __construct()
    {
    }

    public function begin($action = '', $method = 'post', $options = [])
    {
        echo Html::beginForm($action, $method, $options);
        return $this;
    }

    public function end()
    {
        echo Html::endForm();
    }

    public function field($model, $attribute, $options = [])
    {
        $field = new Field($model, $attribute, $options);
        return $field;
    }
}