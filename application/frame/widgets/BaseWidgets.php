<?php

namespace frame\widgets;

/**
* Base class for widgets to inherit from
*/
class BaseWidgets implements IWidgets
{

    function __construct()
    {
    }

    public static function breadcrumbs($breadcrumbs)
    {

    }

    public static function panelBegin($heading, $options = [])
    {

    }

    public static function panelEnd()
    {

    }
}
