<?php

namespace frame\widgets;

/**
* Field templates
*/
class Field
{
    private $model;
    private $attribute;
    private $options;
    private $validation;

    private $parts;

    public $template = "<div class=\"form-control\">{label}\n{input}\n{hint}\n{error}</div>";

    function __construct($model, $attribute, $options = [])
    {
        $this->model = $model;
        $this->attribute = $attribute;
        $this->options = $options;

        $label = $model->getAttributeLabel($attribute);

        $this->addValidation();

        $this->label($label);
    }

    public function addValidation()
    {
        $this->validation = $this->model->getClientValidation($this->attribute);
    }

    public function __toString()
    {
        return $this->render();
    }

    private function render($content = null)
    {
        if ($content === null) {
            if (!isset($this->parts['{input}'])) {
                $this->textInput();
            }
            if (!isset($this->parts['{label}'])) {
                $this->label();
            }
            if (!isset($this->parts['{error}'])) {
                $this->error();
            }
            if (!isset($this->parts['{hint}'])) {
                $this->hint(null);
            }
            $content = strtr($this->template, $this->parts);
        }

        return strtr($this->template, $this->parts);
    }

    public function label($label = null, $options = [])
    {
        $this->parts['{label}'] = Html::label($label, 'theID', $options);
        return $this;
    }

    public function error($options = [])
    {
        $this->parts['{error}'] = empty($error) ? "" : Html::tag("p", $error, $options);
        return $this;
    }

    public function hint($content, $options = [])
    {
        $this->parts['{hint}'] = empty($contnet) ? "" : Html::tag("p", $content, $options);
        return $this;
    }

    public function input($type, $options = [])
    {
    }

    public function textInput($options = [])
    {
        $options = array_merge($options, $this->validation);
        $this->parts['{input}'] = Html::textInput('theName', 999, $options);
        return $this;
    }

    public function hiddenInput($options = [])
    {

    }

    public function passwordInput($options = [])
    {

    }

    public function fileInput($options = [])
    {

    }

    public function textarea($options = [])
    {

    }

    public function radio($options = [], $enclosedByLabel = true)
    {

    }

    public function checkbox($options = [], $enclosedByLabel = true)
    {

    }

    public function dropDownList($items, $options = [])
    {

    }

    public function listBox($items, $options = [])
    {

    }

    public function checkboxList($items, $options = [])
    {

    }

    public function radioList($items, $options = [])
    {

    }
}