<?php

namespace frame\widgets;

use frame\widgets\Html;

/**
* Bootstarap
*/
class BootstrapWidgets implements IWidgets
{

    public static function breadcrumbs($breadcrumbs)
    {
        // no breadcrumbs, return empty string
        if(empty($breadcrumbs)) {
            return "";
        }

        // grab the last crumb (as we dont need a link)
        $last = array_slice($breadcrumbs, -1, 1);
        array_pop($breadcrumbs);

        $html = Html::beginTag('div', ['class' => 'breadcrumb']);

        // output all the crumbs
        if(!empty($breadcrumbs)) {
            foreach ($breadcrumbs as $text => $url) {
                $html .= Html::tag('li', Html::a($text, $url));
            }
        }
        $html .= Html::tag('li', key($last), ['class' => 'active']);

        $html .= Html::endTag('div');
        return $html;
    }


    public static function panelBegin($heading, $options = [])
    {
        $html = "";

        $options['class'] = "panel panel-default" . (empty($options['class']) ? "" : " " . $options['class']);

        $html .= Html::beginTag('div', $options);
        $html .= Html::tag('div', Html::tag('h3', $heading, ['class' => 'panel-title']), ['class' => 'panel-heading']);
        $html .= Html::beginTag('div', ['class' => 'panel-body']);

        return $html;
    }

    public static function panelEnd()
    {
        return Html::endTag('div') . Html::endTag('div');
    }
}