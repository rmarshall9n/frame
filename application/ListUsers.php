<?php

use frame\Frame;
use frame\widgets\BootstrapWidgets as Widgets;
require_once 'Frame/Initialise.php';

Frame::$app->run('User');

// Frame::$app->view->assetManager->loadJSFile('jsfile.js');
// Frame::$app->view->assetManager->loadJSCode('Hi, I\'m Mr Js. Here\'s my lovely code :).');

// Frame::$app->view->assetManager->loadCssFile('cssfile.css');
// Frame::$app->view->assetManager->loadCssCode('Hi, I\'m Mr Css. Here\'s my nice code :).');

Frame::$app->view->title = "List Users";
Frame::$app->view->addBreadcrumbs([
    'index' => 'index.php',
    'users' => 'users.php',
    'List User' => 'listUser.php'
]);

?>
<?= Frame::$app->view->begin(); ?>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <?= Widgets::panelBegin('List of users 1'); ?>
                <p><a href="ViewUsers.php">User 1</a></p>
                <p><a href="ViewUsers.php">User 2</a></p>
                <p><a href="ViewUsers.php">User 3</a></p>
                <p><a href="ViewUsers.php">User 4</a></p>
            <?= Widgets::panelEnd(); ?>
        </div>
        <div class="col-md-6">
            <?= Widgets::panelBegin('List of users 2'); ?>
                <p><a href="ViewUsers.php">User 1</a></p>
                <p><a href="ViewUsers.php">User 2</a></p>
                <p><a href="ViewUsers.php">User 3</a></p>
                <p><a href="ViewUsers.php">User 4</a></p>
            <?= Widgets::panelEnd(); ?>
        </div>
    </div>
</div>

<?= Frame::$app->view->end(); ?>