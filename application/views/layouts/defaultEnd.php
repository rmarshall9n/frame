<?php
use frame\Frame;
?>

    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="pull-left">
                    <p>Copyright &copy; Ryan Marshall <?= date('Y'); ?>.</p>
                </div>
                <div class="pull-right">
                    <p>Built using <?= Frame::$app->getAppName(); ?>.</p>
                </div>
            </div>
        </div>
    </div>

    <?php Frame::$app->view->hookEnd(); ?>
</body>
</html>