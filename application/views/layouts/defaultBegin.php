<?php
use frame\Frame;
use frame\widgets\BootstrapWidgets;
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <?php Frame::$app->view->hookHead(); ?>
</head>
<body>
    <?php Frame::$app->view->hookBegin(); ?>
    <div class="container">
        <div class="row">
            <h1><?= Frame::$app->view->title; ?></h1>
            <br>
            <span>Navigation: </span><a href="/">Home</a>
            <div>
                <?= BootstrapWidgets::breadcrumbs(Frame::$app->view->breadcrumbs); ?>
            </div>
        </div>
    </div>
