
<?php
use frame\Frame;
use frame\base\Response;

require_once 'Frame/Initialise.php';

$title = empty($statusCode) ? "Sorry!": $statusCode . ' - ' . Response::$httpStatuses[$statusCode];

?>
<?= Frame::$app->view->begin(); ?>

    <div class="container">
        <div class="page-header">
            <h1><?= $title; ?></h1>
        </div>

        <h3><?= $message; ?></h3>

        <br>
        <p>If you didn't expect this message, please contact support.</p>
    </div>

<?= Frame::$app->view->end(); ?>