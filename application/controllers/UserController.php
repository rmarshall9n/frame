<?php

namespace controllers;

use frame\Frame;

use frame\Controller;
use models\User;

/**
* User Controller
*/
class UserController extends Controller
{

    function __construct()
    {
        # code...
    }

    public function indexAction()
    {
        return [
            'message' => 'default action',
        ];
    }

    public function listAction()
    {
        return [
            'list1' => 'data1',
            'list2' => 'data2',
            'list3' => 'data3',
        ];
    }

    public function addAction()
    {
        return [];
    }

    public function viewAction()
    {

    }

    public function testAction()
    {

        $user = new User();

        // Frame::dump($user->attributes());
        // Frame::dump($user->hasAttribute('forenamea'));
        // Frame::dump($user->getAttributes());
        // Frame::dump($user->getAttributes(['forename', 'surname'], ['surname']));

        // $user->setAttributes(['forename' => 'ryan']);
        // Frame::dump($user->getAttributes(['forename', 'surname']));

        // $user->surname = 'marshall';
        // Frame::dump($user->getAttributes(['forename', 'surname']));


        // $user->addError('forename', 'Bad name');
        // $user->addError('forename', 'Too long');
        // $user->addError('surname', 'Too long');
        // $user->clearErrors();

        // Frame::dump($user->getFirstError('surname'));
        // Frame::dump($user->getErrors());
        // Frame::dump($user->hasErrors());

        // $user->setAttributes(['forename' => 'ryannnnnnnnnnn']);
        // $user->validate();
        // $user->getErrors();
        // Frame::dump(implode(' ', $user->getClientValidation('forename')));


        echo "Test End.";
        die();


    }
}