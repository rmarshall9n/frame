<?php

namespace controllers;

use frame\Frame;

use frame\Controller;

/**
* Test Controller
*/
class TestController extends Controller
{

    public function indexAction()
    {
        return [
            'message' => 'default action',
        ];
    }

    public function loggerAction()
    {
        return [
        ];
    }


    public function notFoundAction()
    {
        Frame::error()->notFound("bad page");

        return [
            'response'=>"404 error."
        ];
    }



}